/*
 * motion.c
 *
 *  Created on: Jan 22, 2017
 *      Author: brendandonecker
 */
#include "motion.h"

BOOL motion_sample32ms(void) {
static BYTE samples;

  samples <<= 1;
  if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6) == GPIO_PIN_RESET) {
    samples |= 1;
  }
  if ((samples & 0x07) == 0x07) {
    return TRUE;
  }
  return FALSE;
}


