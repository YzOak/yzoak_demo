#include "YzProt.h"


#define CELCIUS 0
#define FARENHEIT 1

//char *Command_Power = {0x50, 0x4F, 0x57., 0x45, 0x52}; //from 0 to 10

#define COMMAND_POWER     0x50
#define COMMAND_TEMP      0x54
#define COMMAND_USAGE     0x55
#define COMMAND_LED       0x4C
#define COMMAND_AMBIENT   0x41
#define COMMAND_INTUITIVE 0x49
#define COMMAND_MOTION    0x4D
#define COMMAND_SECURITY  0x53
#define COMMAND_ONBOARD   0x4F
#define COMMAND_STATUS    
#define COMMAND_INFO      
#define COMMAND_WARNING   0x57
#define COMMAND_EMERGENCY 0x45


BYTE INTUITIVE_STATUS  = 0x00;


//char *Command_Power = "POWER"; //from 0 to 10
//char *Command_Temp = "TEMPERATURE"; //range of thermostat
//char *Command_Usage = "USAGE"; // usage //in watts
//char *Command_Led = "LED"; //color in 6 bit hex number
//char *Command_Ambient = "AMBIENT"; //true or false
//char *Command_Intuitive = "INTUITIVE"; //true or false
//char *Command_Motion = "MOTION"; //true or false
//char *Command_Security= "SECURITY"; //true or false
//char *Command_Onboard = "ONBOARD:"; //
//char *Command_Status = "STATUS"; //request or update
//char *Command_Info = "INFO"; //any text
//char *COMMAND_WARNING = "WARNING"; //any text
//char *Command_Emergency = "EMERGENCY"; //any text with haptic feedback



int watch = 0;
BYTE tmp[8] = {0};
USHORT lenth = 0;
BYTE command = 0x00;
BYTE sub_command = 0x00;


typedef enum {
  YzATTR_NULL = 0,
  YzATTR_TEMPERATURE_C10,
  YzATTR_LIGHT_LEVEL,
  YzATTR_ALLJOYN_RECV,
} YzProt_attribute_t;


typedef struct {
  BYTE pktLen;
  BYTE pktMarshallPtr;
  BYTE pktData[SZ_MAX_PKT];
} YzPkt_t;

YzPkt_t rxPkt;




typedef struct {
   YzPkt_t Temp_Pkt;
   YzPkt_t Motion_Pkt;
}YzFeature_t;


YzFeature_t Feature_Pkt;




YzPkt_t Feature_Output[2];

void YzProt_processPkt(YzPkt_t *);
LONG YzProt_bytesRemaining(YzPkt_t *);
void YzProt_processPktAttribute(YzPkt_t *);
BYTE YzProt_pktReadByte(YzPkt_t *);
USHORT YzProt_pktReadUShort(YzPkt_t *);
ULONG YzProt_pktReadULong(YzPkt_t *);
void YzProt_command_trigger(YzPkt_t *, USHORT);


void YzProt_PwSwitch(char value);
void YzProt_IntuitiveStat(char stat);
void YzProt_InLight(void);


void YzProt_pktOpen1(YzFeature_t *pkt);




//------------------------------------TRANSMITTER---------------------------------------//

//------------------------------------------------------------------------------
void YzProt_pktWriteByte(YzPkt_t *pkt, BYTE data) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  pkt->pktData[pkt->pktMarshallPtr++] = data;
}


//------------------------------------------------------------------------------
void YzProt_pktWriteUShort(YzPkt_t *pkt, USHORT data) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  YzProt_pktWriteByte(pkt, (BYTE)(data >> 8));
  YzProt_pktWriteByte(pkt, (BYTE)(data));
}


//------------------------------------------------------------------------------
void YzProt_pktWriteULong(YzPkt_t *pkt, ULONG data) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  YzProt_pktWriteByte(pkt, (BYTE)(data >> 24));
  YzProt_pktWriteByte(pkt, (BYTE)(data >> 16));
  YzProt_pktWriteByte(pkt, (BYTE)(data >>  8));
  YzProt_pktWriteByte(pkt, (BYTE)(data));
}


//------------------------------------------------------------------------------
void YzProt_pktOpen(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  memset(pkt, 0, sizeof(YzPkt_t));
  YzProt_pktWriteByte(pkt, 0xDE); // some magic identifier
  YzProt_pktWriteByte(pkt, 0xAF); // some magic identifier
  YzProt_pktWriteByte(pkt, 0x01); // message type: post/update
  YzProt_pktWriteByte(pkt, 0x01); // post type: attribute
  YzProt_pktWriteByte(pkt, 0xFF); // checksum_H
  YzProt_pktWriteByte(pkt, 0xFF); // checksum_L
  YzProt_pktWriteByte(pkt, 0xFF); // payload_length_H
  YzProt_pktWriteByte(pkt, 0xFF); // payload_length_L
}


//------------------------------------------------------------------------------
void YzProt_pktClose(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  pkt->pktData[OFFSET_PAYLOAD_LEN_H] = 0x00;
  pkt->pktData[OFFSET_PAYLOAD_LEN_L] = pkt->pktMarshallPtr - SZ_PKT_HDR;
  pkt->pktData[OFFSET_CHECKSUM_H] = 0xFA;
  pkt->pktData[OFFSET_CHECKSUM_L] = 0xDE;

  pkt->pktLen = pkt->pktMarshallPtr;
}


//------------------------------------------------------------------------------
void YzProt_pktWriteAttribute(YzPkt_t *pkt, YzProt_attribute_t attrType, void *attr) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  YzProt_pktWriteUShort(pkt, (USHORT)(attrType)); // Tag

  switch (attrType) {
    case YzATTR_TEMPERATURE_C10:
    case YzATTR_LIGHT_LEVEL:
      YzProt_pktWriteUShort(pkt, 2); // length
      YzProt_pktWriteUShort(pkt, *((USHORT *)attr)); // value
      break;
    default:
    case YzATTR_NULL:
      break;
  }
}


//------------------------------------------------------------------------------
void YzProt_pktSend(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  HAL_UART_Transmit_IT(&huart4, pkt->pktData, (ULONG)pkt->pktLen);
}





//------------------------------------RECEIVER------------------------------------------//
//------------------------------------------------------------------------------
BYTE YzProt_pktReadByte(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Parse an 8-bit number
//------------------------------------------------------------------------------
  if (pkt->pktMarshallPtr >= pkt->pktLen) return 0;
  return (pkt->pktData[pkt->pktMarshallPtr++]);
}


//------------------------------------------------------------------------------
USHORT YzProt_pktReadUShort(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Parse a 16-bit number
//------------------------------------------------------------------------------
  lenth = (USHORT)(YzProt_pktReadByte(pkt)) << 8;
	
	lenth = lenth | (USHORT)(YzProt_pktReadByte(pkt));
	return lenth;
	//return (((USHORT)(YzProt_pktReadByte(pkt)) << 8)
    //    | ((USHORT)(YzProt_pktReadByte(pkt))));
	
}


//------------------------------------------------------------------------------
ULONG YzProt_pktReadULong(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Parse a 32-bit number
//------------------------------------------------------------------------------
  return (((ULONG)(YzProt_pktReadByte(pkt)) << 24)
        | ((ULONG)(YzProt_pktReadByte(pkt)) << 16)
        | ((ULONG)(YzProt_pktReadByte(pkt)) <<  8)
        | ((ULONG)(YzProt_pktReadByte(pkt))));
}



LONG YzProt_bytesRemaining(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Check the number of bytes remaining to be parsed
//------------------------------------------------------------------------------
  return (LONG)pkt->pktLen - (LONG)pkt->pktMarshallPtr;
}


//------------------------------------------------------------------------------
void YzProt_processPktAttribute(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Parse and process an attribute update
//------------------------------------------------------------------------------
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_SET);
	//YzProt_attribute_t attr = (YzProt_attribute_t)YzProt_pktReadUShort(pkt);
	watch = (int)YzProt_pktReadUShort(pkt);
  USHORT attrLen = YzProt_pktReadUShort(pkt);
	switch (watch) {
    case 1:
      (void)YzProt_pktReadUShort(pkt);
      break;
    case 2:
 //     _cbk_attrSetLightLevel(YzProt_pktReadUShort(pkt));
      break;
      case 3:
				HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
				YzProt_command_trigger(pkt,attrLen);
			break;
    case 0:
			break;
    default: // unsupported
      while (attrLen--) YzProt_pktReadByte(pkt);
      break;
  }
	//HAL_UART_Transmit_IT(&huart4, (uint8_t *)attr, 8);
//  switch (attr) {
//    case YzATTR_TEMPERATURE_C10:
//      (void)YzProt_pktReadUShort(pkt);
//      break;
//    case YzATTR_LIGHT_LEVEL:
// //     _cbk_attrSetLightLevel(YzProt_pktReadUShort(pkt));
//      break;
//      case YzATTR_ALLJOYN_RECV:
//				YzProt_command_trigger(pkt,attrLen);
//			break;
//    case YzATTR_NULL:
//    default: // unsupported
//      while (attrLen--) YzProt_pktReadByte(pkt);
//      break;
//  }
}



void YzProt_processPktStart(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Reset all relevant fields in the packet structure so parsing can begin
//------------------------------------------------------------------------------
  pkt->pktMarshallPtr = SZ_PKT_HDR;
}

void YzProt_processPkt(YzPkt_t *pkt) {
//------------------------------------------------------------------------------
// Process a received packet. Header & integrity should be verified before
// calling this function.
//------------------------------------------------------------------------------
  YzProt_processPktStart(pkt);

  while (YzProt_bytesRemaining(pkt)) {
    YzProt_processPktAttribute(pkt);
  }
}



BYTE YzProt_processData(BYTE *data, BYTE len) {
//------------------------------------------------------------------------------
// Process received data
//------------------------------------------------------------------------------
  USHORT payloadLen;

  if (len < SZ_PKT_HDR) return 0;

  if (data[OFFSET_MAGIC_H] != MAGIC_H) return OFFSET_MAGIC_H + 1;
  if (data[OFFSET_MAGIC_L] != MAGIC_L) return OFFSET_MAGIC_L + 1;

  payloadLen = (data[OFFSET_PAYLOAD_LEN_H] << 8) | data[OFFSET_PAYLOAD_LEN_L];
  if (payloadLen == 0)                       return OFFSET_MAGIC_L + 1;
  if (payloadLen  > SZ_MAX_PKT - SZ_PKT_HDR) return OFFSET_MAGIC_L + 1;
  if (payloadLen + SZ_PKT_HDR < len)         return 0; // valid header, but incomplete length

  if (data[OFFSET_CHECKSUM_H] != 0xFA) return OFFSET_MAGIC_L + 1; // add real checksum
  if (data[OFFSET_CHECKSUM_L] != 0xDE) return OFFSET_MAGIC_L + 1;

  rxPkt.pktLen = SZ_PKT_HDR + payloadLen;
  memcpy(rxPkt.pktData, data, rxPkt.pktLen);

  YzProt_processPkt(&rxPkt);
  return SZ_PKT_HDR + payloadLen;
}






//------------------------------------------------------------------------------
void YzProt_pktOpen1(YzFeature_t *pkt) {
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  //memset(pkt, 0, sizeof(YzPkt_t));
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xDE); // some magic identifier
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xAF); // some magic identifier
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0x01); // message type: post/update
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0x01); // post type: attribute
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xFF); // checksum_H
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xFF); // checksum_L
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xFF); // payload_length_H
  YzProt_pktWriteByte(&(pkt->Temp_Pkt), 0xFF); // payload_length_L
}



void Yzprot_SendTemp_Pkt(void) {
   
   YzPkt_t TempPkt;
   
   SHORT YzTemp =  temp_getC10();
   
   YzProt_pktOpen(&(TempPkt));
   YzProt_pktWriteAttribute(&(TempPkt), YzATTR_TEMPERATURE_C10, (void *)(&YzTemp));
   YzProt_pktClose(&(TempPkt));
   
   Feature_Output[0] = TempPkt;
   
   
   YzProt_pktSend(&Feature_Output[0]);
   
   
   
}


//fixed 11/19/17
void YzProt_PwSwitch(char value)
{
   if(value == 0x00)
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_RESET);//reset is switching on //tested on hw using multimeter//port for relay switch ON
   else if(value == 0x01)
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_SET);//set is switching off//port for relay switch OFF
   else;
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_SET);//port for relay switch OFF     
}


void YzProt_IntuitiveStat(char stat)
{
   if(stat == 0x01)
      INTUITIVE_STATUS = 0x01;
   else
      INTUITIVE_STATUS = 0x00;
   
}

//fixit
void YzProt_InLight(void)
{
   if (INTUITIVE_STATUS == 0x01)
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_RESET);//intuitive led ON
   else if(INTUITIVE_STATUS == 0x00)
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_SET);//intuitive led OFF
   else
      HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_SET); 
}




//void extract_string(char *){}
void YzProt_command_trigger(YzPkt_t *pkt, USHORT len)
{

  HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_10);
  
	int l = len;
  int i = 0;

  while(len--)
  {
    tmp[i] = YzProt_pktReadByte(pkt);
		i++;
  }
	//HAL_UART_Transmit_IT(&huart4, (uint8_t*)tmp, l);
  int count = 0;
	i = 0;
  while(tmp[count] != 0x3A)
  {
    command = tmp[count];
    count++;
		i++;
  }

  count +=1;
	i=0;
  while(count != l)
  {
    sub_command = tmp[count];
    count++;
	 i++;
  }
  
	switch(command)
	{
		case COMMAND_POWER:
         YzProt_PwSwitch(sub_command);
			break;
		
		case COMMAND_TEMP:
			break;
		
		case COMMAND_USAGE:
			break;
		
		case COMMAND_LED:
			break;
		
		case COMMAND_AMBIENT:
			break;
		
		case COMMAND_INTUITIVE:
         YzProt_IntuitiveStat(sub_command);
			break;
		
		case COMMAND_SECURITY:
			break;
		
		case COMMAND_ONBOARD:
			break;
		
		case COMMAND_WARNING:
			break;
		
		case COMMAND_EMERGENCY:
			break;
		
		default:
			break;
	}
 
	/*if((command[0] == 0x48) && (command[1] == 0x45))
		HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_11);
	
	if((sub_command[0] == 0x4C) && (sub_command[1] == 0x4F))
		HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_8); */


//  if(strcmp(command, st) == 0)
//  {
//    char *test1 = "REQUEST";
//    char *test2 = "UPDATE";
//      
//    if(strcmp(sub_command, test1) == 0)
//    {
////      HAL_GPIO_WritePin(GPIOE, DEBUG_LED_NW, GPIO_PIN_SET);
//          //send flags data to nanopi
//    }
//          
//    if(strcmp(sub_command, test2) == 0)
//    {
////      HAL_GPIO_WritePin(GPIOE, DEBUG_LED_SW, GPIO_PIN_SET);
//        //update flags data
//    }
//  }

//  //if(strcmp(pkt->pktData, i) == 0)
//    //blink some led
//      
//  //if(strcmp(pkt->pktData, i) == 0)
//    //blink some led

//  //if(strcmp(pkt->pktData, i) == 0)
//    //blink some led
	

}
