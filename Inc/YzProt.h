#include <string.h>
#include "types.h"
#include "stm32f3xx_hal.h"
#include "usart.h"
#include "temp.h"
#include "motion.h"


#define SZ_MAX_PKT (128)
#define SZ_PKT_HDR   (8)
#define MAGIC_H 0xDE
#define MAGIC_L 0xAF
#define OFFSET_MAGIC_H        (0)
#define OFFSET_MAGIC_L        (1)
#define OFFSET_MSG_TYPE       (2)
#define OFFSET_MSG_SUBTYPE    (3)
#define OFFSET_CHECKSUM_H     (4)
#define OFFSET_CHECKSUM_L     (5)
#define OFFSET_PAYLOAD_LEN_H  (6)
#define OFFSET_PAYLOAD_LEN_L  (7)



BYTE YzProt_processData(BYTE *, BYTE);

void Yzprot_SendTemp_Pkt(void);

//remove after test
void YzProt_PwSwitch(char value);

//void Yzprot_SetPower_Pkt(int);

//void YzProt_SendUsage_Pkt(void);

//void YzProt_SetAmbient_Pkt(bool);

//void YzProt_SetMotion_Pkt(bool);

//void YzProt_SetSecurity_Pkt(bool);

//void YzProt_SendStatus_Pkt(void);

//void YzProt_SendWarning_Pkt(void);

//void YzProt_Emergency_Pkt(void);
