/*
 * motion.h
 *
 *  Created on: Jan 22, 2017
 *      Author: brendandonecker
 */

#include "types.h"
#include "stm32f3xx_hal.h"
#ifndef INC_MOTION_H_
#define INC_MOTION_H_

BOOL motion_sample32ms(void);

#endif /* INC_MOTION_H_ */
