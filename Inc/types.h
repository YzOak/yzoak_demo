


#define BOOL unsigned char
#define TRUE 1
#define FALSE 0

#define BYTE uint8_t

#define USHORT unsigned short
#define SHORT signed short

#define ULONG unsigned long
#define LONG signed long
	
//------------------------------------------------------------------------------------------------

#define DEBUG_LED_NW  (GPIO_PIN_8)
#define DEBUG_LED_N   (GPIO_PIN_9)
#define DEBUG_LED_NE  (GPIO_PIN_10)
#define DEBUG_LED_E   (GPIO_PIN_11)
#define DEBUG_LED_SE  (GPIO_PIN_12)
#define DEBUG_LED_S   (GPIO_PIN_13)
#define DEBUG_LED_SW  (GPIO_PIN_14)
#define DEBUG_LED_W   (GPIO_PIN_15)
#define DEBUG_LED_ALL (DEBUG_LED_N | DEBUG_LED_NE | DEBUG_LED_E | DEBUG_LED_SE | DEBUG_LED_S | DEBUG_LED_SW | DEBUG_LED_W | DEBUG_LED_NW)

#define DEBUG_LED_ON(pin)  HAL_GPIO_WritePin(GPIOB, pin, GPIO_PIN_SET)
#define DEBUG_LED_OFF(pin) HAL_GPIO_WritePin(GPIOB, pin, GPIO_PIN_RESET)

#define AMBIENT_SENSOR_ADC_HANDLE (&hadc1)
#define TEMP_SENSOR_ADC_HANDLE  (&hadc2)
#define NANO_PI_UART_HANDLE     (&huart4)
#define DIMMER_DAC_HANDLE       (&hdac)

#define DIMMER_HARD_OFF  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET)
#define DIMMER_HARD_ON   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET)
