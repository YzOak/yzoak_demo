//******************************************************************************
//******************************************************************************
// util.h
// -----------------------------------------------------------------------------
// Author(s):     brendandonecker
// Last Modified: Jan 29, 2017
// -----------------------------------------------------------------------------
// 
//******************************************************************************
//******************************************************************************

#include "types.h"
#include "stm32f3xx_hal.h"

#ifndef INC_UTIL_H_
#define INC_UTIL_H_

BYTE rotl8(BYTE x, BYTE rot);

#endif /* INC_UTIL_H_ */
