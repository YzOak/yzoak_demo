/*
 * temp.h
 *
 *  Created on: Jan 22, 2017
 *      Author: brendandonecker
 */

#ifndef INC_TEMP_H_
#define INC_TEMP_H_


SHORT temp_getC10(void);
SHORT temp_getF10(void);
void temp_tick_32ms(void);
void temp_init(void);

SHORT Ret_Temp(int unit_Temp);


#endif /* INC_TEMP_H_ */
