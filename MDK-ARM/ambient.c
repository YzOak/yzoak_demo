#include "types.h"
#include "stm32f3xx_hal.h"
#include "temp.h"
#include "adc.h"
#include "ambient.h"



int ambient_ret;

//int ambient_val = 0;


USHORT ambient_readCounts(void){

HAL_ADC_Start(AMBIENT_SENSOR_ADC_HANDLE);
  HAL_ADC_PollForConversion(AMBIENT_SENSOR_ADC_HANDLE, HAL_MAX_DELAY);
  ambient_ret = HAL_ADC_GetValue(AMBIENT_SENSOR_ADC_HANDLE);	
  HAL_ADC_Stop(AMBIENT_SENSOR_ADC_HANDLE);
	
	return ambient_ret;
}




///ambient_val = ambient_readCounts();
